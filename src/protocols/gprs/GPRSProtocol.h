/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_GPRS_GPRSPROTOCOL_H_
#define SRC_PROTOCOLS_GPRS_GPRSPROTOCOL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "Protocol.h"
#include "CacheManager.h"
#include "GPRSInfo.h"
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include "flow/FlowManager.h"

namespace aiengine {

// Minimum GPRS header, for data and signaling
typedef struct {
        uint8_t flags;          // Flags 
        uint8_t type;       	// Message type 
        uint16_t length;        // Length of data
	uint32_t teid;
        u_char data[0];         //
} __attribute__((packed)) gprs_hdr;

// Minimum PDP Context Request
typedef struct {
	uint16_t seq_num;	// Sequence number
	uint8_t n_pdu;		// N-PDU 
	uint8_t code;
	uint8_t presence;
	union {
		struct { // For extension header
			u_char hdr[4];
			uint64_t imsi;
		} __attribute__((packed)) ext;
		struct { // Regular header
			uint64_t imsi;
			u_char hdr[4];
		} __attribute__((packed)) reg;
	} un;	
	u_char data[0]; 
} __attribute__((packed)) gprs_create_pdp_hdr;

typedef struct {
	u_char tid_data[5];
	u_char tid_control_plane[5];
	u_char nsapi[2];
	u_char data[0];
} __attribute__((packed)) gprs_create_pdp_hdr_ext;

// Routing area identity header 0x03
typedef struct {
        uint16_t mcc;           // Mobile Country Code
        uint16_t mnc;           // Mobile Network Code
        uint16_t lac;
        uint8_t rac;
	u_char data[0];
} __attribute__((packed)) gprs_create_pdp_hdr_routing;

// GPRS Extension header 
typedef struct {
        uint8_t length;           // Length of the extension
        uint16_t seq;           //e
        uint8_t next_hdr;
} __attribute__((packed)) gprs_extension_hdr;

#define GPRS_ECHO_REQUEST 1
#define GPRS_ECHO_RESPONSE 2 
#define CREATE_PDP_CONTEXT_REQUEST 16 
#define	CREATE_PDP_CONTEXT_RESPONSE 17
#define	UPDATE_PDP_CONTEXT_REQUEST 18
#define	UPDATE_PDP_CONTEXT_RESPONSE 19
#define	DELETE_PDP_CONTEXT_REQUEST 20
#define	DELETE_PDP_CONTEXT_RESPONSE 21 
#define	T_PDU 255 

class GPRSProtocol: public Protocol {
public:
    	explicit GPRSProtocol();
    	virtual ~GPRSProtocol(); 

	static const uint16_t id = 0;
	static const int header_size = 8; // GTP version 1
	int getHeaderSize() const { return header_size; }

	void processFlow(Flow *flow);
	bool processPacket(Packet& packet) { return true; } // Nothing to process

	void statistics(std::basic_ostream<char>& out) { statistics(out, stats_level_); }
	void statistics(std::basic_ostream<char>& out, int level);

        void setMultiplexer(MultiplexerPtrWeak mux) { mux_ = mux; }
        MultiplexerPtrWeak getMultiplexer() { return mux_;}

        void setFlowForwarder(WeakPointer<FlowForwarder> ff) { flow_forwarder_= ff; }
        WeakPointer<FlowForwarder> getFlowForwarder() { return flow_forwarder_; }

	void releaseCache(); // Release the objets attached to the flows 

        void setHeader(unsigned char *raw_packet) {
       
		gprs_header_ = reinterpret_cast<gprs_hdr*>(raw_packet); 
        }

	// Condition for say that a packet is GPRS 
	bool gprsChecker(Packet& packet); 
	
	//unsigned char *getPayload() const { return &gprs_header_; }
	uint8_t getType() const { return gprs_header_->type; }
	uint16_t getHeaderLength() const { return ntohs(gprs_header_->length); }
	bool haveSequenceNumber() const { return (gprs_header_->flags & (1 << 1)); }
	bool haveExtensionHeader() const { return (gprs_header_->flags & (1 << 2)); }

        void increaseAllocatedMemory(int value) { gprs_info_cache_->create(value); }
        void decreaseAllocatedMemory(int value) { gprs_info_cache_->destroy(value); }

	void setFlowManager(FlowManagerPtrWeak flow_mng) { flow_mng_ = flow_mng; }

	int64_t getCurrentUseMemory() const;
	int64_t getAllocatedMemory() const; 
	int64_t getTotalAllocatedMemory() const; 

        void setDynamicAllocatedMemory(bool value);
        bool isDynamicAllocatedMemory() const; 

	int32_t getTotalCacheMisses() const;

	CounterMap getCounters() const; 

	void setCacheManager(SharedPointer<CacheManager> cmng) { cache_mng_ = cmng; cache_mng_->setCache(gprs_info_cache_); }
private:

	void process_create_pdp_context(Flow *flow);

	Cache<GPRSInfo>::CachePtr gprs_info_cache_;
	gprs_hdr *gprs_header_;
	int32_t total_create_pdp_ctx_requests_;
	int32_t total_create_pdp_ctx_responses_;
	int32_t total_update_pdp_ctx_requests_;
	int32_t total_update_pdp_ctx_responses_;
	int32_t total_delete_pdp_ctx_requests_;
	int32_t total_delete_pdp_ctx_responses_;
	int32_t total_tpdus_;
	int32_t total_echo_requests_;
	int32_t total_echo_responses_;
	FlowManagerPtrWeak flow_mng_;
	SharedPointer<CacheManager> cache_mng_;
};

typedef std::shared_ptr<GPRSProtocol> GPRSProtocolPtr;

} // namespace aiengine 

#endif  // SRC_PROTOCOLS_GPRS_GPRSPROTOCOL_H_
