/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "test_snmp.h"

#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE_TEST
#define BOOST_TEST_MODULE snmptest
#endif
#include <boost/test/unit_test.hpp>

using namespace aiengine;

BOOST_FIXTURE_TEST_SUITE(snmp_test_suite, StackSNMPtest)

BOOST_AUTO_TEST_CASE (test01)
{
        Packet packet;

        BOOST_CHECK(snmp->getTotalPackets() == 0);
        BOOST_CHECK(snmp->getTotalValidatedPackets() == 0);
        BOOST_CHECK(snmp->getTotalBytes() == 0);
        BOOST_CHECK(snmp->getTotalMalformedPackets() == 0);
	BOOST_CHECK(snmp->processPacket(packet) == true);
	
	CounterMap c = snmp->getCounters();
}

BOOST_AUTO_TEST_CASE (test02)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_snmp_get);
        int length = raw_packet_ethernet_ip_udp_snmp_get_length;
        Packet packet(pkt,length);

	inject(packet);

        BOOST_CHECK(ip->getTotalPackets() == 1);
        BOOST_CHECK(ip->getTotalValidatedPackets() == 1);
        BOOST_CHECK(ip->getTotalBytes() == 48 +8 + 20);
        BOOST_CHECK(ip->getTotalMalformedPackets() == 0);

        BOOST_CHECK(snmp->getTotalPackets() == 1);
        BOOST_CHECK(snmp->getTotalValidatedPackets() == 1);
        BOOST_CHECK(snmp->getTotalBytes() == 48);
        BOOST_CHECK(snmp->getTotalMalformedPackets() == 0);

	BOOST_CHECK(snmp->getTotalEvents() == 0);	
}

BOOST_AUTO_TEST_CASE (test03)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_snmpv2_resp);
        int length = raw_packet_ethernet_ip_udp_snmpv2_resp_length;
        Packet packet(pkt, length);

	inject(packet);

        BOOST_CHECK(ip->getTotalPackets() == 1);
        BOOST_CHECK(ip->getTotalValidatedPackets() == 1);
        BOOST_CHECK(ip->getTotalBytes() == 42 + 8 + 20);
        BOOST_CHECK(ip->getTotalMalformedPackets() == 0);
        
	BOOST_CHECK(snmp->getTotalPackets() == 1);
        BOOST_CHECK(snmp->getTotalValidatedPackets() == 1);
        BOOST_CHECK(snmp->getTotalBytes() == 42);
        BOOST_CHECK(snmp->getTotalMalformedPackets() == 0);

	BOOST_CHECK(snmp->getTotalEvents() == 0);	
}

BOOST_AUTO_TEST_CASE (test04) // Corrupt the snmp packet
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_snmpv2_resp);
        int length = raw_packet_ethernet_ip_udp_snmpv2_resp_length;
        Packet packet(pkt, length - 12);

        auto flow = SharedPointer<Flow>(new Flow());

        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);
        snmp->processFlow(flow.get());

	// Verify the anomaly
        BOOST_CHECK(flow->getPacketAnomaly() == PacketAnomalyType::SNMP_BOGUS_HEADER);
	BOOST_CHECK(snmp->getTotalEvents() == 1);	
}

BOOST_AUTO_TEST_CASE (test05)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_snmpv2_set);
        int length = raw_packet_ethernet_ip_udp_snmpv2_set_length;
        Packet packet(pkt, length);

	inject(packet);

        BOOST_CHECK(ip->getTotalPackets() == 1);
        BOOST_CHECK(ip->getTotalValidatedPackets() == 1);
        BOOST_CHECK(ip->getTotalBytes() == 136 + 8 + 20);
        BOOST_CHECK(ip->getTotalMalformedPackets() == 0);
        
	BOOST_CHECK(snmp->getTotalPackets() == 1);
        BOOST_CHECK(snmp->getTotalValidatedPackets() == 1);
        BOOST_CHECK(snmp->getTotalBytes() == 136);
        BOOST_CHECK(snmp->getTotalMalformedPackets() == 0);

	BOOST_CHECK(snmp->getTotalEvents() == 0);	
}

BOOST_AUTO_TEST_CASE (test06) // malformed community length
{
	struct snmp_hdr hsnmp;
	hsnmp.code = 0; 
	hsnmp.length = 8;
	hsnmp.type = SNMP_SET_REQ; 
	hsnmp.version_length = 2;
	u_char buffer[32];

        unsigned char *pkt = reinterpret_cast <unsigned char*> (&hsnmp);
	std::memcpy(&buffer,&hsnmp,sizeof(struct snmp_hdr));
	std::memcpy(&buffer[sizeof(struct snmp_hdr)],"\x00\xaa\x04\xfa",4);

        int length = sizeof(struct snmp_hdr) + 4;

        Packet packet(buffer, length);

        auto flow = SharedPointer<Flow>(new Flow());

        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);
	
        snmp->processFlow(flow.get());

	// Verify the anomaly
        BOOST_CHECK(flow->getPacketAnomaly() == PacketAnomalyType::SNMP_BOGUS_HEADER);
	BOOST_CHECK(snmp->getTotalEvents() == 1);	
}

BOOST_AUTO_TEST_CASE (test07)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_snmp_get_next_request);
        int length = raw_packet_ethernet_ip_udp_snmp_get_next_request_length;
        Packet packet(pkt, length);

	inject(packet);

        BOOST_CHECK(ip->getTotalPackets() == 1);
        BOOST_CHECK(ip->getTotalValidatedPackets() == 1);
        BOOST_CHECK(ip->getTotalBytes() == 37 + 8 + 20);
        BOOST_CHECK(ip->getTotalMalformedPackets() == 0);
        
	BOOST_CHECK(snmp->getTotalPackets() == 1);
        BOOST_CHECK(snmp->getTotalValidatedPackets() == 1);
        BOOST_CHECK(snmp->getTotalBytes() == 37);
        BOOST_CHECK(snmp->getTotalMalformedPackets() == 0);
	BOOST_CHECK(snmp->getTotalEvents() == 0);	
}

BOOST_AUTO_TEST_SUITE_END()
