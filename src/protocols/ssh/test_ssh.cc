/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "test_ssh.h"

#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE_TEST
#define BOOST_TEST_MODULE sshtest
#endif
#include <boost/test/unit_test.hpp>

using namespace aiengine;

BOOST_FIXTURE_TEST_SUITE(ssh_test_suite, StackSSHtest)

BOOST_AUTO_TEST_CASE (test01)
{
	Packet packet;

	BOOST_CHECK(ssh->getTotalBytes() == 0);
	BOOST_CHECK(ssh->getTotalPackets() == 0);
	BOOST_CHECK(ssh->getTotalValidatedPackets() == 0);
	BOOST_CHECK(ssh->getTotalMalformedPackets() == 0);
	BOOST_CHECK(ssh->processPacket(packet) == true);
	
	CounterMap c = ssh->getCounters();

	BOOST_CHECK(ssh->isDynamicAllocatedMemory() == false);

	auto v1 = ssh->getCurrentUseMemory();
	auto v2 = ssh->getAllocatedMemory();
	auto v3 = ssh->getTotalAllocatedMemory();

	BOOST_CHECK(ssh->getTotalCacheMisses() == 0);

	ssh->decreaseAllocatedMemory(10);
}

BOOST_AUTO_TEST_CASE (test02)
{
	unsigned char *pkt1 = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_ssh_client_paramiko_1);
        int length1 = raw_packet_ethernet_ip_tcp_ssh_client_paramiko_1_length;
	unsigned char *pkt2 = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_ssh_server_paramiko_1);
        int length2 = raw_packet_ethernet_ip_tcp_ssh_server_paramiko_1_length;
        Packet packet1(pkt1, length1);
        Packet packet2(pkt2, length2);

	inject(packet1);

        Flow *flow = ssh->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(info->isHandshake() == true);
	BOOST_CHECK(info->getTotalEncryptedBytes() == 0);

	BOOST_CHECK(ssh->getTotalBytes() == 25);
	BOOST_CHECK(ssh->getTotalPackets() == 1);
	BOOST_CHECK(ssh->getTotalValidatedPackets() == 1);
	BOOST_CHECK(ssh->getTotalMalformedPackets() == 0);

	inject(packet2);

        flow = ssh->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(info->isHandshake() == true);
	BOOST_CHECK(info->getTotalEncryptedBytes() == 0);

	BOOST_CHECK(ssh->getTotalBytes() == 25 + 21);
	BOOST_CHECK(ssh->getTotalPackets() == 2);
	BOOST_CHECK(ssh->getTotalValidatedPackets() == 1);
	BOOST_CHECK(ssh->getTotalMalformedPackets() == 0);
	BOOST_CHECK(ssh->getTotalHandshakePDUs() == 0);

        BOOST_CHECK(ssh->getTotalAlgorithmNegotiationMessages() == 0);
        BOOST_CHECK(ssh->getTotalKeyExchangeMessages() == 0);
        BOOST_CHECK(ssh->getTotalOthers() == 0);
        BOOST_CHECK(ssh->getTotalEncryptedBytes() == 0);

	// Force a release
	releaseFlow(flow);
}

BOOST_AUTO_TEST_CASE (test03)
{
        auto flow = SharedPointer<Flow>(new Flow());
        unsigned char *pkt = reinterpret_cast <unsigned char*> (&raw_packet_ethernet_ip_tcp_ssh_client_key_exchange_1[66]);
        int length = raw_packet_ethernet_ip_tcp_ssh_client_key_exchange_1_length;
        Packet packet(pkt, length - 66);

	flow->total_packets_l7 = 3;
        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);

        ssh->processFlow(flow.get());

	BOOST_CHECK(ssh->getTotalBytes() == 600);
	BOOST_CHECK(ssh->getTotalPackets() == 1);
	BOOST_CHECK(ssh->getTotalHandshakePDUs() == 1);
        
        BOOST_CHECK(ssh->getTotalAlgorithmNegotiationMessages() == 1);
        BOOST_CHECK(ssh->getTotalKeyExchangeMessages() == 0);
        BOOST_CHECK(ssh->getTotalOthers() == 0);
}

BOOST_AUTO_TEST_CASE (test04)
{
        auto flow = SharedPointer<Flow>(new Flow());
        unsigned char *pkt = reinterpret_cast <unsigned char*> (&raw_packet_ethernet_ip_tcp_ssh_server_key_exchange_1[66]);
        int length = raw_packet_ethernet_ip_tcp_ssh_server_key_exchange_1_length;
        Packet packet(pkt, length - 66);

	flow->total_packets_l7 = 4;
        flow->setFlowDirection(FlowDirection::BACKWARD);
        flow->packet = const_cast<Packet*>(&packet);

        ssh->processFlow(flow.get());

	BOOST_CHECK(ssh->getTotalBytes() == 784);
	BOOST_CHECK(ssh->getTotalPackets() == 1);
	BOOST_CHECK(ssh->getTotalHandshakePDUs() == 1);
	
        BOOST_CHECK(ssh->getTotalAlgorithmNegotiationMessages() == 1);
        BOOST_CHECK(ssh->getTotalKeyExchangeMessages() == 0);
        BOOST_CHECK(ssh->getTotalOthers() == 0);
        BOOST_CHECK(ssh->getTotalEncryptedBytes() == 0);
}

BOOST_AUTO_TEST_CASE (test05)
{
        auto flow = SharedPointer<Flow>(new Flow());
        unsigned char *pkt = reinterpret_cast <unsigned char*> (&raw_packet_ethernet_ip_tcp_ssh_client_diffie_hellman_key_exchange_1[66]);
        int length = raw_packet_ethernet_ip_tcp_ssh_client_diffie_hellman_key_exchange_1_length;
        Packet packet(pkt, length - 66);

	flow->total_packets_l7 = 4;
        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);

        ssh->processFlow(flow.get());

	BOOST_CHECK(ssh->getTotalBytes() == 144);
	BOOST_CHECK(ssh->getTotalPackets() == 1);
	BOOST_CHECK(ssh->getTotalHandshakePDUs() == 1);
	
        BOOST_CHECK(ssh->getTotalAlgorithmNegotiationMessages() == 0);
        BOOST_CHECK(ssh->getTotalKeyExchangeMessages() == 1);
        BOOST_CHECK(ssh->getTotalOthers() == 0);
        BOOST_CHECK(ssh->getTotalEncryptedBytes() == 0);
}

BOOST_AUTO_TEST_CASE (test06)
{
        auto flow = SharedPointer<Flow>(new Flow());
        unsigned char *pkt = reinterpret_cast <unsigned char*> (&raw_packet_ethernet_ip_tcp_ssh_server_diffie_hellman_key_exchange_1[66]);
        int length = raw_packet_ethernet_ip_tcp_ssh_server_diffie_hellman_key_exchange_1_length;
        Packet packet(pkt, length - 66);

	flow->total_packets_l7 = 4;
        flow->setFlowDirection(FlowDirection::BACKWARD);
        flow->packet = const_cast<Packet*>(&packet);

        ssh->processFlow(flow.get());

	BOOST_CHECK(ssh->getTotalBytes() == 720);
	BOOST_CHECK(ssh->getTotalPackets() == 1);
	BOOST_CHECK(ssh->getTotalHandshakePDUs() == 2);
	
        BOOST_CHECK(ssh->getTotalAlgorithmNegotiationMessages() == 1);
        BOOST_CHECK(ssh->getTotalKeyExchangeMessages() == 1);
        BOOST_CHECK(ssh->getTotalOthers() == 0);
        BOOST_CHECK(ssh->getTotalEncryptedBytes() == 0);

	SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);

	// The next packet from the server will be encrypted
	BOOST_CHECK(info->isServerHandshake() == false);
}

BOOST_AUTO_TEST_CASE (test07)
{
        auto flow = SharedPointer<Flow>(new Flow());
        unsigned char *pkt = reinterpret_cast <unsigned char*> (&raw_packet_ethernet_ip_tcp_ssh_client_new_keys_1[66]);
        int length = raw_packet_ethernet_ip_tcp_ssh_client_new_keys_1_length;
        Packet packet(pkt, length - 66);

	flow->total_packets_l7 = 4;
        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);

        ssh->processFlow(flow.get());

	BOOST_CHECK(ssh->getTotalBytes() == 16);
	BOOST_CHECK(ssh->getTotalPackets() == 1);
	BOOST_CHECK(ssh->getTotalHandshakePDUs() == 1);
	
        BOOST_CHECK(ssh->getTotalAlgorithmNegotiationMessages() == 1);
        BOOST_CHECK(ssh->getTotalKeyExchangeMessages() == 0);
        BOOST_CHECK(ssh->getTotalOthers() == 0);
        BOOST_CHECK(ssh->getTotalEncryptedBytes() == 0);
	
	SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);

	// The client will send the next packet encrypted
	BOOST_CHECK(info->isClientHandshake() == false);
	BOOST_CHECK(info->getTotalEncryptedBytes() == 0);
	
	BOOST_CHECK(ssh->getTotalEncryptedBytes() == 0);
	BOOST_CHECK(ssh->getTotalEncryptedPackets() == 0);
}

BOOST_AUTO_TEST_CASE (test08) 
{
        unsigned char *pkt1 = reinterpret_cast <unsigned char*> (&raw_packet_ethernet_ip_tcp_ssh_server_diffie_hellman_key_exchange_1[66]);
        int length1 = raw_packet_ethernet_ip_tcp_ssh_server_diffie_hellman_key_exchange_1_length;
        Packet packet1(pkt1, length1 - 66);

        unsigned char *pkt2 = reinterpret_cast <unsigned char*> (&raw_packet_ethernet_ip_tcp_ssh_client_new_keys_1[66]);
        int length2 = raw_packet_ethernet_ip_tcp_ssh_client_new_keys_1_length;
        Packet packet2(pkt2, length2 - 66);

        unsigned char *pkt3 = reinterpret_cast <unsigned char*> (&raw_packet_ethernet_ip_tcp_ssh_client_encrypted_packet_1[66]);
        int length3 = raw_packet_ethernet_ip_tcp_ssh_client_encrypted_packet_1_length;
        Packet packet3(pkt3, length3 - 66);
	
        auto flow = SharedPointer<Flow>(new Flow());

	ssh->setDynamicAllocatedMemory(true);

	flow->total_packets_l7 = 4;
        flow->setFlowDirection(FlowDirection::BACKWARD);
        flow->packet = const_cast<Packet*>(&packet1);

	// Inject the server packet
        ssh->processFlow(flow.get());

	SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(info->isClientHandshake() == true);
	BOOST_CHECK(info->isServerHandshake() == false);
	BOOST_CHECK(info->isHandshake() == true);
	BOOST_CHECK(info->getTotalEncryptedBytes() == 0);
        
	flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet2);
       
	// Inject the client packet 
	ssh->processFlow(flow.get());

	BOOST_CHECK(info->isClientHandshake() == false);
	BOOST_CHECK(info->isServerHandshake() == false);
	BOOST_CHECK(info->isHandshake() == false);
	BOOST_CHECK(info->getTotalEncryptedBytes() == 0);

	flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet3);

	// Inject the client encrypted packet        
	ssh->processFlow(flow.get());

	// The number of encrypted bytes should be non zero

	BOOST_CHECK(info->isClientHandshake() == false);
	BOOST_CHECK(info->isServerHandshake() == false);
	BOOST_CHECK(info->isHandshake() == false);
	BOOST_CHECK(info->getTotalEncryptedBytes() == 48);
	
	BOOST_CHECK(ssh->getTotalEncryptedBytes() == 48);
	BOOST_CHECK(ssh->getTotalEncryptedPackets() == 1);

        JsonFlow j;
        info->serialize(j);

        std::filebuf fb;
        fb.open ("/dev/null",std::ios::out);
        std::ostream outp(&fb);
        flow->serialize(outp);
        flow->showFlowInfo(outp);
        outp << *(info.get());
	ssh->statistics(outp, 5);
        fb.close();
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_FIXTURE_TEST_SUITE(ipv6_ssh_test_suite, StackIPv6SSHtest)

BOOST_AUTO_TEST_CASE (test01)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip6_tcp_ssh_server_cisco_2);
        int length = raw_packet_ethernet_ip6_tcp_ssh_server_cisco_2_length;
        Packet packet(pkt, length);

        inject(packet);

        Flow *flow = ssh->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);

        BOOST_CHECK(info->isHandshake() == true);
        BOOST_CHECK(info->getTotalEncryptedBytes() == 0);

        BOOST_CHECK(ssh->getTotalBytes() == 19);
        BOOST_CHECK(ssh->getTotalPackets() == 1);
        BOOST_CHECK(ssh->getTotalValidatedPackets() == 1);
        BOOST_CHECK(ssh->getTotalMalformedPackets() == 0);

        JsonFlow j;
        info->serialize(j);

        std::filebuf fb;
        fb.open ("/dev/null",std::ios::out);
        std::ostream outp(&fb);
        flow->serialize(outp);
        flow->showFlowInfo(outp);
        outp << *(info.get());

        ssh->statistics(outp, 5);
        fb.close();

	ssh->releaseCache();
}

BOOST_AUTO_TEST_CASE (test02)
{
        auto flow = SharedPointer<Flow>(new Flow());
        unsigned char *pkt = reinterpret_cast <unsigned char*> (&raw_packet_ethernet_ip6_tcp_ssh_server_new_keys_2[74]);
        int length = raw_packet_ethernet_ip6_tcp_ssh_server_new_keys_2_length;
        Packet packet(pkt, length - 74);

	flow->total_packets_l7 = 4;
        flow->setFlowDirection(FlowDirection::BACKWARD);
        flow->packet = const_cast<Packet*>(&packet);

        ssh->processFlow(flow.get());

	BOOST_CHECK(ssh->getTotalBytes() == 16);
	BOOST_CHECK(ssh->getTotalPackets() == 1);
	BOOST_CHECK(ssh->getTotalHandshakePDUs() == 1);
	
        BOOST_CHECK(ssh->getTotalAlgorithmNegotiationMessages() == 1);
        BOOST_CHECK(ssh->getTotalKeyExchangeMessages() == 0);
        BOOST_CHECK(ssh->getTotalOthers() == 0);
        BOOST_CHECK(ssh->getTotalEncryptedBytes() == 0);
	
	SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);
	
	BOOST_CHECK(info->isClientHandshake() == true);
	BOOST_CHECK(info->isServerHandshake() == false);
	BOOST_CHECK(info->isHandshake() == true);
	BOOST_CHECK(info->getTotalEncryptedBytes() == 0);
}

BOOST_AUTO_TEST_CASE (test03)
{
        auto flow = SharedPointer<Flow>(new Flow());
        unsigned char *pkt = reinterpret_cast <unsigned char*> (&raw_packet_ethernet_ip6_tcp_ssh_client_new_keys_2[74]);
        int length = raw_packet_ethernet_ip6_tcp_ssh_client_new_keys_2_length;
        Packet packet(pkt, length - 74);

	flow->total_packets_l7 = 4;
        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);

        ssh->processFlow(flow.get());

	BOOST_CHECK(ssh->getTotalBytes() == 16);
	BOOST_CHECK(ssh->getTotalPackets() == 1);
	BOOST_CHECK(ssh->getTotalHandshakePDUs() == 1);
	
        BOOST_CHECK(ssh->getTotalAlgorithmNegotiationMessages() == 1);
        BOOST_CHECK(ssh->getTotalKeyExchangeMessages() == 0);
        BOOST_CHECK(ssh->getTotalOthers() == 0);
        BOOST_CHECK(ssh->getTotalEncryptedBytes() == 0);
	
	SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);
	
	BOOST_CHECK(info->isClientHandshake() == false);
	BOOST_CHECK(info->isServerHandshake() == true);
	BOOST_CHECK(info->isHandshake() == true);
	BOOST_CHECK(info->getTotalEncryptedBytes() == 0);
}

BOOST_AUTO_TEST_CASE (test04) // Memory failure test
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip6_tcp_ssh_server_cisco_2);
        int length = raw_packet_ethernet_ip6_tcp_ssh_server_cisco_2_length;
        Packet packet(pkt, length);

	ssh->decreaseAllocatedMemory(10);

        inject(packet);

        Flow *flow = ssh->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info == nullptr);
}

BOOST_AUTO_TEST_CASE (test05) // Bogus length on the packet
{
        auto flow = SharedPointer<Flow>(new Flow());
        unsigned char *pkt = reinterpret_cast <unsigned char*> (&raw_packet_ethernet_ip6_tcp_ssh_client_new_keys_2[74]);
        int length = raw_packet_ethernet_ip6_tcp_ssh_client_new_keys_2_length;
	// Modify the length 
	unsigned char buffer[128];

	std::memcpy(buffer, pkt, 128);
	buffer[1] = '\xff';
	buffer[2] = '\xff';
	buffer[3] = '\xff';
        Packet packet((unsigned char*)&buffer, length - 74);

	flow->total_packets_l7 = 4;
        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);

        ssh->processFlow(flow.get());

        SharedPointer<SSHInfo> info = flow->getSSHInfo();
        BOOST_CHECK(info != nullptr);
	
	BOOST_CHECK(ssh->getTotalPackets() == 1);
	BOOST_CHECK(ssh->getTotalHandshakePDUs() == 0);
}

BOOST_AUTO_TEST_SUITE_END()
