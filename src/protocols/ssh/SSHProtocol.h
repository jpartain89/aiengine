/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_SSH_SSHPROTOCOL_H_
#define SRC_PROTOCOLS_SSH_SSHPROTOCOL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_LIBLOG4CXX
#include "log4cxx/logger.h"
#endif
#include "Protocol.h"
#include "flow/FlowManager.h"
#include "Cache.h"
#include <arpa/inet.h>

namespace aiengine {

typedef struct __attribute__((packed)) {
	uint32_t length;   	/* payload length */
    	uint8_t padding;        /* padding */
	u_char data[0];       
} ssh_hdr;

// Some of the most common message types

class SSHProtocol: public Protocol {
public:
    	explicit SSHProtocol();
    	virtual ~SSHProtocol();

	static const uint16_t id = 0;	
	static constexpr int header_size = sizeof(ssh_hdr);

	int getHeaderSize() const { return header_size;}

        void processFlow(Flow *flow);
        bool processPacket(Packet& packet) { return true; } 

	void statistics(std::basic_ostream<char>& out) { statistics(out, stats_level_); }
	void statistics(std::basic_ostream<char>& out, int level);

	void releaseCache(); 

	void setHeader(unsigned char *raw_packet) { 

		ssh_header_ = raw_packet;
	}

	// Condition for say that a packet is ssh 
	bool sshChecker(Packet &packet); 
	
	// Protocol specific

	int64_t getCurrentUseMemory() const; 
	int64_t getAllocatedMemory() const;
	int64_t getTotalAllocatedMemory() const;

        void setDynamicAllocatedMemory(bool value);
        bool isDynamicAllocatedMemory() const;

        int32_t getTotalCacheMisses() const;

        void increaseAllocatedMemory(int value);
        void decreaseAllocatedMemory(int value);

        void setFlowManager(FlowManagerPtrWeak flow_mng) { flow_mng_ = flow_mng; }

	CounterMap getCounters() const; 

	Flow *getCurrentFlow() const { return current_flow_; }

	void setCacheManager(SharedPointer<CacheManager> cmng) { cache_mng_ = cmng; cache_mng_->setCache(info_cache_); }

#if defined(STAND_ALONE_TEST) || defined(TESTING)
        int32_t getTotalHandshakePDUs() const { return total_handshake_pdus_; }
	int32_t getTotalAlgorithmNegotiationMessages() const { return total_algorithm_negotiation_messages_; }
	int32_t getTotalKeyExchangeMessages() const { return total_key_exchange_messages_; }
	int32_t getTotalOthers() const { return total_others_; }

	int64_t getTotalEncryptedBytes() const { return total_encrypted_bytes_; }
	int32_t getTotalEncryptedPackets() const { return total_encrypted_packets_; }
#endif

private:
	bool is_minimal_ssh_header(const unsigned char *hdr);

	unsigned char *ssh_header_;

	// Some statistics 
	int64_t total_encrypted_bytes_;
	int32_t total_encrypted_packets_;
	int32_t total_handshake_pdus_;
	int32_t total_algorithm_negotiation_messages_; // messages from 20 to 29
	int32_t total_key_exchange_messages_; // messages from 30 to 49
	int32_t total_others_;

	Cache<SSHInfo>::CachePtr info_cache_;

	FlowManagerPtrWeak flow_mng_;
        Flow *current_flow_;
	SharedPointer<CacheManager> cache_mng_;
#ifdef HAVE_LIBLOG4CXX
        static log4cxx::LoggerPtr logger;
#endif
};

typedef std::shared_ptr<SSHProtocol> SSHProtocolPtr;

} // namespace aiengine

#endif  // SRC_PROTOCOLS_SSH_SSHPROTOCOL_H_
