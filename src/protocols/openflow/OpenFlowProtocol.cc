/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "OpenFlowProtocol.h"
#include <iomanip>

namespace aiengine {

OpenFlowProtocol::OpenFlowProtocol():
	Protocol("OpenFlowProtocol", "openflow"),
	of_header_(nullptr),
	total_ofp_hellos_(0),
	total_ofp_feature_requests_(0),
	total_ofp_feature_replys_(0),
	total_ofp_set_configs_(0),
	total_ofp_packets_in_(0),
	total_ofp_packets_out_(0) 
	{}

bool OpenFlowProtocol::openflowChecker(Packet &packet) {

	int length = packet.getLength();

	if (length >= header_size) {
		setHeader(packet.getPayload());

		if ((of_header_->version >= OF_VERSION_1)and(of_header_->version <= OF_VERSION_1_3)) {
			++total_validated_packets_;
			return true;
		}
	}
	++total_malformed_packets_;
	return false;
}


void OpenFlowProtocol::process_packet_in(MultiplexerPtr mux, Packet *packet) {

	unsigned char *payload = packet->getPayload();
	int hdr_size = sizeof(openflow_v1_pktin_hdr);

	if (of_header_->version == OF_VERSION_1) {
		hdr_size = sizeof(openflow_v1_pktin_hdr);
	} else if (of_header_->version == OF_VERSION_1_3) {
		openflow_v13_pktin_hdr *inpkt = reinterpret_cast<openflow_v13_pktin_hdr*>(payload);
		hdr_size = sizeof(openflow_v13_pktin_hdr) + ntohs(inpkt->match_length) + 2;
	} else {
		//TODO not supported
		return;
	}

	if (hdr_size < packet->getLength()) {	
		Packet gpacket(&payload[hdr_size], packet->getLength() - hdr_size);

		gpacket.setPrevHeaderSize(0);
		mux->setHeaderSize(0);
		mux->setNextProtocolIdentifier(0);
		mux->forwardPacket(gpacket);
	}
}

void OpenFlowProtocol::process_packet_out(MultiplexerPtr mux, Packet *packet) {

	int bytes = packet->getLength();
	int offset = 0;
	unsigned char *payload = packet->getPayload();
	int hdr_size = sizeof(openflow_v1_pktout_hdr);
	uint8_t version = of_header_->version;
	uint16_t length = ntohs(of_header_->length);

	do {
		int pkt_offset = offset;
		int olength = 0;
		if (version == OF_VERSION_1) {
			hdr_size = sizeof(openflow_v1_pktout_hdr);
			openflow_v1_pktout_hdr *outpkt = reinterpret_cast<openflow_v1_pktout_hdr*>(&payload[offset]);
			length = ntohs(outpkt->hdr.length);
			pkt_offset = offset + hdr_size;
			olength = length - hdr_size;
		} else if (version == OF_VERSION_1_3) {
			openflow_v13_pktout_hdr *outpkt = reinterpret_cast<openflow_v13_pktout_hdr*>(&payload[offset]);
			length = ntohs(outpkt->hdr.length);
			hdr_size = sizeof(openflow_v13_pktout_hdr) + ntohs(outpkt->actions_length);
			pkt_offset = offset + hdr_size;
			olength = length - hdr_size;
		} else {
			// TODO not supported openflow version
			break;
		}

		if (pkt_offset < bytes) { // offset on the boundaries
			int real_length = bytes - pkt_offset;
			if (olength <= real_length) { // The olength should be minor or equal to the real length	
				Packet gpacket(&payload[pkt_offset], olength);

				gpacket.setPrevHeaderSize(0);
				mux->setHeaderSize(0);
				mux->setNextProtocolIdentifier(0);
				mux->forwardPacket(gpacket);
			}
		}

		offset += length;
	} while (offset < bytes);
}

void OpenFlowProtocol::processFlow(Flow *flow) {

	int bytes = flow->packet->getLength();
	total_bytes_ += bytes;
	++total_packets_;

	if (mux_.lock()&&(bytes >= header_size)) {
		MultiplexerPtr mux = mux_.lock();

                Packet *packet = flow->packet;
		setHeader(packet->getPayload());

		uint8_t version = of_header_->version;
		uint8_t type = of_header_->type;

		if ((version >= OF_VERSION_1)and(version <= OF_VERSION_1_3)) {
			if (type == OFP_PACKET_IN) { // Message that contains a packet to forward
				process_packet_in(mux, packet);
				++total_ofp_packets_in_;
			} else if (type == OFP_PACKET_OUT) {
				process_packet_out(mux, packet);
				++total_ofp_packets_out_;
			} else if (type == OFP_HELLO ) {
				++total_ofp_hellos_;
			} else if (type == OFP_FEATURE_REQUEST) {
				++total_ofp_feature_requests_;
			} else if (type == OFP_FEATURE_REPLY) {
				++total_ofp_feature_replys_;
			} else if (type == OFP_SET_CONFIG) {
				++total_ofp_set_configs_;
			}
		}
         }
}

void OpenFlowProtocol::statistics(std::basic_ostream<char> &out, int level) { 

	if (level > 0) {
                int64_t alloc_memory = getAllocatedMemory();
                std::string unit = "Bytes";

                unitConverter(alloc_memory, unit);

                out << getName() << "(" << this <<") statistics" << std::dec << std::endl;
                out << "\t" << "Total allocated:        " << std::setw(9 - unit.length()) << alloc_memory << " " << unit << std::endl;
		out << "\t" << "Total packets:          " << std::setw(10) << total_packets_ << std::endl;
		out << "\t" << "Total bytes:        " << std::setw(14) << total_bytes_ << std::endl;
		if (level > 1) {
			out << "\t" << "Total validated packets:" << std::setw(10) << total_validated_packets_ << std::endl;
			out << "\t" << "Total malformed packets:" << std::setw(10) << total_malformed_packets_ << std::endl;
                        if (level > 3) {
                                out << "\t" << "Total hellos:           " << std::setw(10) << total_ofp_hellos_ << std::endl;
                                out << "\t" << "Total feature requests: " << std::setw(10) << total_ofp_feature_requests_ << std::endl;
                                out << "\t" << "Total feature replys:   " << std::setw(10) << total_ofp_feature_replys_ << std::endl;
                                out << "\t" << "Total set configs:      " << std::setw(10) << total_ofp_set_configs_ << std::endl;
                                out << "\t" << "Total packets in:       " << std::setw(10) << total_ofp_packets_in_ << std::endl;
                                out << "\t" << "Total packets out:      " << std::setw(10) << total_ofp_packets_out_ << std::endl;
                       	} 
			if (level > 2) {
				if (mux_.lock())
					mux_.lock()->statistics(out);
                                if (flow_forwarder_.lock())
                                        flow_forwarder_.lock()->statistics(out);
			}
		}
	}
}

CounterMap OpenFlowProtocol::getCounters() const {
  	CounterMap cm; 

        cm.addKeyValue("packets", total_packets_);
        cm.addKeyValue("bytes", total_bytes_);
        cm.addKeyValue("hellos", total_ofp_hellos_);
        cm.addKeyValue("feature requests", total_ofp_feature_requests_);
        cm.addKeyValue("feature replys", total_ofp_feature_replys_);
        cm.addKeyValue("set configs", total_ofp_set_configs_);
        cm.addKeyValue("packets in", total_ofp_packets_in_);
        cm.addKeyValue("packets out", total_ofp_packets_out_);

        return cm;
}

} // namespace aiengine
