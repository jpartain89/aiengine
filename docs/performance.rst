In this section we are going to explore and compare the different performance values such as CPU and memory comsumption
with other engines such as tshark, snort and suricata.

The pcap file use is from (http://www.unb.ca/cic/research/datasets/index.html) is aproximately 17GB size with the mayority of traffic HTTP.

On the other hand, the tools used for evaluate the performance and comsumption with be perf and massif.

Snort version used 2.9.9.0
Tshark version 2.0.2
AIEngine version 1.7.0
Suricata version 3.2.1
nDPI version 2.1.0

The machine is a 8 CPUS Intel(R) Core(TM) i7-6820HQ CPU @ 2.70GHz with 16 GB memory. 


Processing traffic
~~~~~~~~~~~~~~~~~~

In this section we explore how fast are the engines just processing the traffic without any rules or any logic on them.

Snort
*****

.. code:: bash

   Performance counter stats for './snort -r /pcaps/iscx/testbed-17jun.pcap -c ./snort.conf':

      64269.015098      task-clock (msec)         #    0.981 CPUs utilized          
             1,760      context-switches          #    0.027 K/sec                  
                36      cpu-migrations            #    0.001 K/sec                  
            44,841      page-faults               #    0.698 K/sec                  
   204,394,163,771      cycles                    #    3.180 GHz                    
   375,256,677,520      instructions              #    1.84  insns per cycle        
    98,031,161,725      branches                  # 1525.325 M/sec                  
       565,404,035      branch-misses             #    0.58% of all branches        

      65.487290231 seconds time elapsed


Tshark
******

.. code:: bash

   Performance counter stats for 'tshark -q -z conv,tcp -r /pcaps/iscx/testbed-17jun.pcap':

     112070.498904      task-clock (msec)         #    0.909 CPUs utilized          
            11,390      context-switches          #    0.102 K/sec                  
               261      cpu-migrations            #    0.002 K/sec                  
         2,172,942      page-faults               #    0.019 M/sec                  
   310,196,020,123      cycles                    #    2.768 GHz                    
   449,687,949,322      instructions              #    1.45  insns per cycle        
    99,620,662,743      branches                  #  888.911 M/sec                  
       729,598,416      branch-misses             #    0.73% of all branches        

     123.265736897 seconds time elapsed

Suricata
********

With 9 packet processing threads

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml -r /pcaps/iscx/testbed-17jun.pcap':

     100446.349460      task-clock (msec)         #    3.963 CPUs utilized          
         2,264,381      context-switches          #    0.023 M/sec                  
           220,905      cpu-migrations            #    0.002 M/sec                  
           108,722      page-faults               #    0.001 M/sec                  
   274,824,170,581      cycles                    #    2.736 GHz                    
   249,152,605,118      instructions              #    0.91  insns per cycle        
    56,052,176,697      branches                  #  558.031 M/sec                  
       538,776,158      branch-misses             #    0.96% of all branches        

      25.345742192 seconds time elapsed

With one packet processing thread

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml --runmode single -r /pcaps/iscx/testbed-17jun.pcap':

      94797.134432      task-clock (msec)         #    1.989 CPUs utilized          
           124,424      context-switches          #    0.001 M/sec                  
             1,158      cpu-migrations            #    0.012 K/sec                  
            71,535      page-faults               #    0.755 K/sec                  
   261,166,110,590      cycles                    #    2.755 GHz                    
   306,188,504,447      instructions              #    1.17  insns per cycle        
    72,333,018,827      branches                  #  763.030 M/sec                  
       468,673,879      branch-misses             #    0.65% of all branches        

      47.668130400 seconds time elapsed

nDPI
****

.. code:: bash

   Performance counter stats for './ndpiReader -i /pcaps/iscx/testbed-17jun.pcap':

      20134.419533      task-clock (msec)         #    0.758 CPUs utilized          
            78,990      context-switches          #    0.004 M/sec                  
               104      cpu-migrations            #    0.005 K/sec                  
            44,408      page-faults               #    0.002 M/sec                  
    55,566,151,984      cycles                    #    2.760 GHz                    
    62,980,097,786      instructions              #    1.13  insns per cycle        
    15,048,874,292      branches                  #  747.420 M/sec                  
       281,671,995      branch-misses             #    1.87% of all branches        

      26.559667812 seconds time elapsed

AIengine
********

.. code:: bash

    Performance counter stats for './aiengine -i /pcaps/iscx/testbed-17jun.pcap -o':

      18039.084521      task-clock (msec)         #    0.704 CPUs utilized          
            91,186      context-switches          #    0.005 M/sec                  
               476      cpu-migrations            #    0.026 K/sec                  
            17,430      page-faults               #    0.966 K/sec                  
    50,049,479,531      cycles                    #    2.775 GHz                    
    67,835,601,309      instructions              #    1.36  insns per cycle        
    14,522,520,700      branches                  #  805.059 M/sec                  
       166,645,083      branch-misses             #    1.15% of all branches        

      25.610078128 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cicles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       | 204,394M |     375,256M |      65 |
+-------------+----------+--------------+---------+
| Tshark      | 310,196M |      99,620M |     123 |
+-------------+----------+--------------+---------+
| Suricata(1) | 261,166M |     306,188M |      47 |
+-------------+----------+--------------+---------+
| Suricata(9) | 274,824M |     249,152M |      25 |
+-------------+----------+--------------+---------+
| nDPI        |  55,566M |      62,980M |      26 |
+-------------+----------+--------------+---------+
| AIEngine    |  50,049M |      67,835M |      25 |
+-------------+----------+--------------+---------+

Tests with rules
~~~~~~~~~~~~~~~~

On this section we evalute simple rules in order to compare the different systems.

The rule that we are going to use is quite simple, it consists on find the string "cmd.exe" on the payload of all the TCP traffic.

Snort
*****

.. code:: bash

   alert tcp any any -> any any (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1)

.. code:: bash

   Performance counter stats for './snort -r /pcaps/iscx/testbed-17jun.pcap -c ./snort.conf':

     271091.019789      task-clock (msec)         #    0.994 CPUs utilized          
             3,213      context-switches          #    0.012 K/sec                  
                80      cpu-migrations            #    0.000 K/sec                  
            65,124      page-faults               #    0.240 K/sec                  
   731,608,435,272      cycles                    #    2.699 GHz                    
 1,033,203,748,622      instructions              #    1.41  insns per cycle        
   193,558,431,134      branches                  #  713.998 M/sec                  
       655,588,320      branch-misses             #    0.34% of all branches        

     272.704320602 seconds time elapsed

Suricata
********

.. code:: bash

   alert tcp any any -> any any (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1; rev:1;)

With 9 packet processing threads

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml -r /pcaps/iscx/testbed-17jun.pcap':

     147104.764348      task-clock (msec)         #    4.864 CPUs utilized          
         1,380,685      context-switches          #    0.009 M/sec                  
            49,927      cpu-migrations            #    0.339 K/sec                  
           388,670      page-faults               #    0.003 M/sec                  
   404,341,193,048      cycles                    #    2.749 GHz                    
   426,566,148,876      instructions              #    1.05  insns per cycle        
    80,421,852,312      branches                  #  546.698 M/sec                  
       624,570,278      branch-misses             #    0.78% of all branches        

      30.242149664 seconds time elapsed

With one packet processing thread

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml --runmode single -r /pcaps/iscx/testbed-17jun.pcap':

     158579.888281      task-clock (msec)         #    1.976 CPUs utilized          
            97,030      context-switches          #    0.612 K/sec                  
             1,143      cpu-migrations            #    0.007 K/sec                  
            52,539      page-faults               #    0.331 K/sec                  
   442,028,848,482      cycles                    #    2.787 GHz                    
   591,840,610,271      instructions              #    1.34  insns per cycle        
   125,011,110,377      branches                  #  788.316 M/sec                  
       493,436,768      branch-misses             #    0.39% of all branches        

      80.250462424 seconds time elapsed

AIEngine
********

Rule: "cmd.exe"

.. code:: bash

   Performance counter stats for './aiengine -i /pcaps/iscx/testbed-17jun.pcap -R -r cmd.exe -m -c tcp':

      25815.755873      task-clock (msec)         #    0.945 CPUs utilized          
            38,008      context-switches          #    0.001 M/sec                  
               109      cpu-migrations            #    0.004 K/sec                  
             2,587      page-faults               #    0.100 K/sec                  
    79,739,426,930      cycles                    #    3.089 GHz                    
   170,908,065,937      instructions              #    2.14  insns per cycle        
    48,678,620,025      branches                  # 1885.617 M/sec                  
       438,618,038      branch-misses             #    0.90% of all branches        

      27.323182412 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cicles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       | 731,608M |   1,033,203M |     272 |
+-------------+----------+--------------+---------+
| Suricata(1) | 442,028M |     591,840M |      80 |
+-------------+----------+--------------+---------+
| Suricata(9) | 404,341M |     426,566M |      30 |
+-------------+----------+--------------+---------+
| AIEngine    |  79,739M |     170,908M |      27 |
+-------------+----------+--------------+---------+

Snort
*****

A simliar rules as before but just trying to help a bit to Snort.

.. code:: bash

   alert tcp any any -> any 80 (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1; rev:1;)

.. code:: bash

   Performance counter stats for './snort -r /pcaps/iscx/testbed-17jun.pcap -c ./snort.conf':

      70456.213488      task-clock (msec)         #    0.984 CPUs utilized          
             5,901      context-switches          #    0.084 K/sec                  
                63      cpu-migrations            #    0.001 K/sec                  
            79,927      page-faults               #    0.001 M/sec                  
   214,846,354,228      cycles                    #    3.049 GHz                    
   385,107,871,838      instructions              #    1.79  insns per cycle        
   100,011,250,526      branches                  # 1419.481 M/sec                  
       579,460,528      branch-misses             #    0.58% of all branches        

      71.582493144 seconds time elapsed

Suricata
********

Change the rule just for HTTP traffic

.. code:: bash

   alert http any any -> any any (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1; rev:1;)

With 9 processing packet threads

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml -r /pcaps/iscx/testbed-17jun.pcap':

     140314.604419      task-clock (msec)         #    5.007 CPUs utilized          
         1,326,047      context-switches          #    0.009 M/sec                  
            81,882      cpu-migrations            #    0.584 K/sec                  
           287,767      page-faults               #    0.002 M/sec                  
   385,297,597,444      cycles                    #    2.746 GHz                    
   427,295,175,085      instructions              #    1.11  insns per cycle        
    80,682,776,679      branches                  #  575.013 M/sec                  
       570,289,598      branch-misses             #    0.71% of all branches        

      28.023789653 seconds time elapsed

With one processing packet thread

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml --runmode single -r /pcaps/iscx/testbed-17jun.pcap':

     148652.663600      task-clock (msec)         #    1.974 CPUs utilized          
            96,622      context-switches          #    0.650 K/sec                  
               637      cpu-migrations            #    0.004 K/sec                  
            53,167      page-faults               #    0.358 K/sec                  
   426,698,526,702      cycles                    #    2.870 GHz                    
   591,218,425,219      instructions              #    1.39  insns per cycle        
   124,816,600,210      branches                  #  839.653 M/sec                  
       475,639,059      branch-misses             #    0.38% of all branches        

      75.314408592 seconds time elapsed


AIEngine
********

.. code:: python

  def http_callback(flow):
    print("rule on HTTP %s" % str(flow))

  if __name__ == '__main__':

    st = StackLan()

    http = DomainNameManager() 
    rm = RegexManager()
    r = Regex("my cmd.exe", "cmd.exe")
    r.callback = anomaly_callback

    d1 = DomainName("Generic net",".net")
    d2 = DomainName("Generic com",".com")
    d3 = DomainName("Generic org",".org")
 
    http.add_domain_name(d1) 
    http.add_domain_name(d2) 
    http.add_domain_name(d3) 

    d1.regex_manager = rm
    d2.regex_manager = rm
    d3.regex_manager = rm

    rm.add_regex(r)

    st.set_domain_name_manager(http, "HTTPProtocol")

    st.set_dynamic_allocated_memory(True)
    
    with pyaiengine.PacketDispatcher("/pcaps/iscx/testbed-17jun.pcap") as pd:
            pd.stack = st
            pd.run()

    sys.exit(0)

.. code:: bash

   Performance counter stats for 'python http_cmd.exe.py':

      23225.111783      task-clock (msec)         #    0.852 CPUs utilized          
            65,407      context-switches          #    0.003 M/sec                  
               229      cpu-migrations            #    0.010 K/sec                  
            11,765      page-faults               #    0.507 K/sec                  
    65,522,914,305      cycles                    #    2.821 GHz                    
   107,810,319,801      instructions              #    1.65  insns per cycle        
    28,075,084,777      branches                  # 1208.825 M/sec                  
       282,404,757      branch-misses             #    1.01% of all branches        

      27.252074897 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cicles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       | 214,846M |     385,107M |      71 |
+-------------+----------+--------------+---------+
| Suricata(1) | 426,698M |     591,840M |      75 |
+-------------+----------+--------------+---------+
| Suricata(9) | 385,297M |     591,218M |      28 |
+-------------+----------+--------------+---------+
| AIEngine    |  65,522M |     107,810M |      27 |
+-------------+----------+--------------+---------+

Tests with 31.000 rules
~~~~~~~~~~~~~~~~~~~~~~~

On this section we evalute aproximatelly 31.000 rules in order to compare the different systems.
Basically we load 31.000 different domains on each engine and loaded into memory and compare the performance.

Snort
*****

.. code:: bash
   
   alert tcp any any -> any 80 (content:"lb.usemaxserver.de"; msg:"Traffic"; sid:1; rev:1;)
   ....

.. code:: bash

   Performance counter stats for './snort -r /pcaps/iscx/testbed-17jun.pcap -c ./snort.conf':

     239911.454192      task-clock (msec)         #    0.994 CPUs utilized          
             1,866      context-switches          #    0.008 K/sec                  
                29      cpu-migrations            #    0.000 K/sec                  
           275,912      page-faults               #    0.001 M/sec                  
   730,183,866,577      cycles                    #    3.044 GHz                    
   523,549,153,058      instructions              #    0.72  insns per cycle        
   151,703,407,200      branches                  #  632.331 M/sec                  
       784,133,786      branch-misses             #    0.52% of all branches        

     241.344591225 seconds time elapsed

Suricata
********

.. code:: bash

   alert http any any -> any any (content:"lb.usemaxserver.de"; http_host; msg:"Traffic"; sid:1; rev:1;)
   ....

With 9 processing packet threads

.. code:: bash

   Performance counter stats for './suricata -r /pcaps/iscx/testbed-17jun.pcap -c suricata.yaml':

     129366.651117      task-clock (msec)         #    3.812 CPUs utilized          
         1,484,897      context-switches          #    0.011 M/sec                  
           115,294      cpu-migrations            #    0.891 K/sec                  
           347,011      page-faults               #    0.003 M/sec                  
   354,238,365,666      cycles                    #    2.738 GHz                    
   330,226,571,287      instructions              #    0.93  insns per cycle        
    81,479,451,099      branches                  #  629.834 M/sec                  
       598,088,820      branch-misses             #    0.73% of all branches        

      33.935354390 seconds time elapsed

With one single packet thread

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml --runmode single -r /pcaps/iscx/testbed-17jun.pcap':

     137079.150338      task-clock (msec)         #    1.872 CPUs utilized          
           101,577      context-switches          #    0.741 K/sec                  
             1,481      cpu-migrations            #    0.011 K/sec                  
           291,789      page-faults               #    0.002 M/sec                  
   370,552,220,742      cycles                    #    2.703 GHz                    
   443,891,171,842      instructions              #    1.20  insns per cycle        
   112,343,969,730      branches                  #  819.555 M/sec                  
       518,724,581      branch-misses             #    0.46% of all branches        

      73.230102972 seconds time elapsed

nDPI
****

.. code:: bash

   host:"lb.usemaxserver.de"@MyProtocol

.. code:: bash

   Performance counter stats for './ndpiReader -p http_ndpi.rules -i /pcaps/iscx/testbed-17jun.pcap':

      21913.851054      task-clock (msec)         #    0.779 CPUs utilized          
            59,037      context-switches          #    0.003 M/sec                  
                83      cpu-migrations            #    0.004 K/sec                  
           716,580      page-faults               #    0.033 M/sec                  
    59,048,108,901      cycles                    #    2.695 GHz                    
    63,994,766,870      instructions              #    1.08  insns per cycle        
    15,288,226,665      branches                  #  697.651 M/sec                  
       284,549,749      branch-misses             #    1.86% of all branches        

      28.147959104 seconds time elapsed

AIEngine
********

.. code:: bash

   h = pyaiengine.DomainName("domain_1" % i, "b.usemaxserver.de")
   h.callback = http_callback
   dm.add_domain_name(h)
   ....

The version used on this test is 1.8.x (master)

.. code:: bash

   Performance counter stats for 'python demo5.py':

      19910.218719      task-clock (msec)         #    0.753 CPUs utilized          
            86,698      context-switches          #    0.004 M/sec                  
             2,013      cpu-migrations            #    0.101 K/sec                  
            18,189      page-faults               #    0.914 K/sec                  
    53,190,204,685      cycles                    #    2.672 GHz                    
    64,288,671,886      instructions              #    1.21  insns per cycle        
    13,373,079,549      branches                  #  671.669 M/sec                  
       169,799,323      branch-misses             #    1.27% of all branches        

      26.452084280 seconds time elapsed


+-------------+----------+--------------+---------+
| Test        | Cicles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       | 730,183M |     523,549M |     241 |
+-------------+----------+--------------+---------+
| Suricata(1) | 370,552M |     443,891M |      73 |
+-------------+----------+--------------+---------+
| Suricata(9) | 354,238M |     330,226M |      33 |
+-------------+----------+--------------+---------+
| nDPI        |  59,048M |      63,994M |      28 |
+-------------+----------+--------------+---------+
| AIEngine    |  53,190M |      64,288M |      26 |
+-------------+----------+--------------+---------+

Now we are going to make a complex rule.

The idea is to analyze the HTTP uri and search for a word in our case "exe".

Snort
*****

.. code:: bash

   alert tcp any any -> any 80 (content:"lb.usemaxserver.de"; uricontent:"exe"; msg:"Traffic"; sid:1; rev:1;)
   ....

.. code:: bash

   Performance counter stats for './snort -r /pcaps/iscx/testbed-17jun.pcap -c ./snort.conf':

      76455.475108      task-clock (msec)         #    0.981 CPUs utilized          
             3,594      context-switches          #    0.047 K/sec                  
                99      cpu-migrations            #    0.001 K/sec                  
           111,397      page-faults               #    0.001 M/sec                  
   229,619,037,994      cycles                    #    3.003 GHz                    
   405,962,474,441      instructions              #    1.77  insns per cycle        
   106,466,397,876      branches                  # 1392.528 M/sec                  
       594,124,564      branch-misses             #    0.56% of all branches        

      77.938067412 seconds time elapsed

Suricata
********

.. code:: bash

   alert http any any -> any any (content:"lb.usemaxserver.de"; http_host; conent:"exe"; http_uri; msg:"Traffic"; sid:1; rev:1;)
   ....

With 9 processing packet threads

.. code:: bash

   Performance counter stats for './suricata -r /pcaps/iscx/testbed-17jun.pcap -c suricata.yaml':

     123037.997614      task-clock (msec)         #    3.475 CPUs utilized          
         1,765,919      context-switches          #    0.014 M/sec                  
           148,475      cpu-migrations            #    0.001 M/sec                  
           353,585      page-faults               #    0.003 M/sec                  
   332,912,328,748      cycles                    #    2.706 GHz                    
   332,626,051,284      instructions              #    1.00  insns per cycle        
    81,934,929,717      branches                  #  665.932 M/sec                  
       592,853,289      branch-misses             #    0.72% of all branches        

      35.411677796 seconds time elapsed

With one single packet thread

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml --runmode single -r /pcaps/iscx/testbed-17jun.pcap':

     111133.956719      task-clock (msec)         #    1.843 CPUs utilized          
           111,599      context-switches          #    0.001 M/sec                  
             1,077      cpu-migrations            #    0.010 K/sec                  
           306,054      page-faults               #    0.003 M/sec                  
   310,127,777,799      cycles                    #    2.791 GHz                    
   412,013,001,291      instructions              #    1.33  insns per cycle        
   103,895,197,621      branches                  #  934.865 M/sec                  
       508,998,872      branch-misses             #    0.49% of all branches        

      60.309266689 seconds time elapsed

AIEngine
********

.. code:: bash

   rm = pyaiengine.RegexManager()
   r = pyaiengine.Regex("on the uri", "^.*(exe).*$")
   rm.add_regex(r)

   h = pyaiengine.DomainName("domain_1" % i, "b.usemaxserver.de")
   h.callback = http_callback
   h.http_uri_regex_manager = rm
   dm.add_domain_name(h)
   ....

The version used on this test is 1.8.x (master)

.. code:: bash

   Performance counter stats for 'python demo5.py':

      19910.218719      task-clock (msec)         #    0.753 CPUs utilized          
            86,698      context-switches          #    0.004 M/sec                  
             2,013      cpu-migrations            #    0.101 K/sec                  
            18,189      page-faults               #    0.914 K/sec                  
    53,190,204,685      cycles                    #    2.672 GHz                    
    64,288,671,886      instructions              #    1.21  insns per cycle        
    13,373,079,549      branches                  #  671.669 M/sec                  
       169,799,323      branch-misses             #    1.27% of all branches        

      26.452084280 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cicles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       | 229,619M |     405,962M |      77 |
+-------------+----------+--------------+---------+
| Suricata(1) | 310,127M |     412,013M |      60 |
+-------------+----------+--------------+---------+
| Suricata(9) | 332,912M |     332,626M |      35 |
+-------------+----------+--------------+---------+
| AIEngine    |  53,190M |      64,288M |      26 |
+-------------+----------+--------------+---------+


Another tests by making more complex the rule 

The idea is to analyze the HTTP uri and search for different words(exe, bat and png).

Snort
*****

.. code:: bash

   alert tcp any any -> any 80 (content:"lb.usemaxserver.de"; pcre:"/^.*(exe|bat|png).*$/"; msg:"Traffic"; sid:1; rev:1;)

.. code:: bash 

   Commencing packet processing (pid=11951)
   ===============================================================================
   Run time for packet processing was 87.8067 seconds
   Snort processed 17310684 packets.
   Snort ran for 0 days 0 hours 1 minutes 27 seconds
      Pkts/min:     17310684
      Pkts/sec:       198973

   ...

   Performance counter stats for './snort -r /pcaps/iscx/testbed-17jun.pcap -c ./snort.conf':

     332419.465677      task-clock (msec)         #    0.996 CPUs utilized          
             1,897      context-switches          #    0.006 K/sec                  
                70      cpu-migrations            #    0.000 K/sec                  
           298,836      page-faults               #    0.899 K/sec                  
   870,336,957,271      cycles                    #    2.618 GHz                    
   527,446,002,353      instructions              #    0.61  insns per cycle        
   152,281,712,268      branches                  #  458.101 M/sec                  
       771,410,918      branch-misses             #    0.51% of all branches        

     333.678629049 seconds time elapsed

The packet processing takes bout 88 seconds but the full load of the rules takes a long time, probably due to the use of the pcre.

Suricata
********

.. code:: bash

   alert http any any -> any any (content:"lb.usemaxserver.de"; http_host; pcre:"/^.*(exe|bat|png).*$/"; msg:"Traffic"; sid:1; rev:1;)

With 9 processing packet threads

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml -r /pcaps/iscx/testbed-17jun.pcap':

     133747.431539      task-clock (msec)         #    3.796 CPUs utilized          
         1,507,433      context-switches          #    0.011 M/sec                  
           123,806      cpu-migrations            #    0.926 K/sec                  
           374,176      page-faults               #    0.003 M/sec                  
   362,046,514,184      cycles                    #    2.707 GHz                    
   335,210,037,408      instructions              #    0.93  insns per cycle        
    82,517,301,739      branches                  #  616.964 M/sec                  
       598,287,782      branch-misses             #    0.73% of all branches        

      35.237027328 seconds time elapsed


Running suricata with one single thread (same has AIEngine)

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml --runmode single -r /pcaps/iscx/testbed-17jun.pcap':

     122334.651821      task-clock (msec)         #    1.864 CPUs utilized          
            97,856      context-switches          #    0.800 K/sec                  
             1,073      cpu-migrations            #    0.009 K/sec                  
           300,312      page-faults               #    0.002 M/sec                  
   344,624,244,835      cycles                    #    2.817 GHz                    
   439,114,648,308      instructions              #    1.27  insns per cycle        
   110,921,840,589      branches                  #  906.708 M/sec                  
       513,286,800      branch-misses             #    0.46% of all branches        

      65.636419341 seconds time elapsed

AIEngine
********

The version used on this test is 1.8.x(master)

By using the or exclusive on the regex

.. code:: python

   rm = pyaiengine.RegexManager()
   r = pyaiengine.Regex("on the uri", "^.*(exe|png|bat).*$")
   rm.add_regex(r)

   h = pyaiengine.DomainName("domain_1" % i, "b.usemaxserver.de")
   h.callback = http_callback
   h.http_uri_regex_manager = rm
   dm.add_domain_name(h)
   ....

.. code:: bash

   Performance counter stats for 'python demo5.py':

      20363.823330      task-clock (msec)         #    0.768 CPUs utilized          
            83,882      context-switches          #    0.004 M/sec                  
             2,623      cpu-migrations            #    0.129 K/sec                  
            18,598      page-faults               #    0.913 K/sec                  
    53,956,703,007      cycles                    #    2.650 GHz                    
    64,766,501,001      instructions              #    1.20  insns per cycle        
    13,439,249,412      branches                  #  659.957 M/sec                  
       172,814,792      branch-misses             #    1.29% of all branches        

      26.520199336 seconds time elapsed


Creating three different regex

.. code:: python

   rm = pyaiengine.RegexManager()
   r1 = pyaiengine.Regex("on the uri1", "^.*(exe).*$")
   r2 = pyaiengine.Regex("on the uri2", "^.*(png).*$")
   r3 = pyaiengine.Regex("on the uri3", "^.*(bat).*$")
   rm.add_regex(r1)
   rm.add_regex(r2)
   rm.add_regex(r3)


.. code:: bash

   Performance counter stats for 'python demo5.py':

      19791.762693      task-clock (msec)         #    0.754 CPUs utilized          
            86,195      context-switches          #    0.004 M/sec                  
             2,433      cpu-migrations            #    0.123 K/sec                  
            18,554      page-faults               #    0.937 K/sec                  
    52,834,415,115      cycles                    #    2.670 GHz                    
    64,730,476,443      instructions              #    1.23  insns per cycle        
    13,448,104,589      branches                  #  679.480 M/sec                  
       173,179,979      branch-misses             #    1.29% of all branches        

      26.252955656 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cicles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       | 870,336M |     527,446M |      87 |
+-------------+----------+--------------+---------+
| Suricata(1) | 344,624M |     439,114M |      65 |
+-------------+----------+--------------+---------+
| Suricata(9) | 362,046M |     335,210M |      35 |
+-------------+----------+--------------+---------+
| AIEngine    |  52,834M |      64,730M |      26 |
+-------------+----------+--------------+---------+



